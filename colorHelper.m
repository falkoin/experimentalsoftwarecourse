%% colors

color.black = [0, 0, 0];
color.white = [255, 255, 255];
color.red = [255, 0, 0];
color.green = [0, 255, 0];
color.blue = [0, 0, 255];
color.yellow = [255, 255, 0];
color.cyan = [0, 255, 255];
color.magenta = [255, 0, 255];
color.midGrey = [128, 128, 128];
color.silver = [64, 64, 64];
color.darkGrey = [192, 192, 192];
color.maroon = [128, 0, 0];
color.olive = [128, 128, 0];
color.darkGreen = [0, 128, 0];
color.purple = [128, 0, 128];
color.teal = [0, 128, 128];
color.navy = [0, 0, 128];
