% keyPressSolution03.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)


%% Settings
Screen('Preference', 'SkipSyncTests', 1);
whichScreen = 0;
whichKeyboard = -1;
keys.off = 2;
keys.on = 0;
load( '../colorHelper.mat' ) % predefined colors

% Experiment settings
delayTimes = 0.5 : 0.25 : 1.5; % possible random foreperiods
nDelays = numel( delayTimes ); % check number of elements
randomDelay = delayTimes(randi( nDelays )); % pick a random foreperiod value

try
  %% Window
  [mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen, [], [0, 0 , 1280, 800] ); % opens PTB window
  screenWidth = windowRect(3);
  screenHeight = windowRect(4);
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  %% Audiooptions
  % Sets options for items to be played
  audioVolume = 0.1;
  [audioData, audioFrequency] = psychwavread( 'beep-02.wav' ); % load soundfile
  waveData = audioData'; % convert audiodata
  % checks if the file was mono or stereo. If it's mono the channel will be doubled to fake stereo
  nChannels = size( waveData, 1 );
  if nChannels < 2 % if mono
    waveData = [waveData; waveData]; % make stereo
    nChannels = 2;
  end
  
  %% Audio initialization
  InitializePsychSound(); % init sound
  latencySetting = 2;
  audioHandle = PsychPortAudio( 'Open', [], [], latencySetting, audioFrequency, 2 ); % create an audio handle
  
  PsychPortAudio( 'FillBuffer', audioHandle, waveData ); % prefill the buffer for fast access
  PsychPortAudio( 'Volume', audioHandle, audioVolume ); % set volume of the sound output
  
  
  %% Plays items
  WaitSecs( randomDelay ); % random foreperiod
  timeStimulus = PsychPortAudio( 'Start', audioHandle, 1, 0, 1 ); % play the audiofile from the buffer
  
  %% Pause
  timeResponse = KbWait( whichKeyboard ); % halts program until any key is pressed
  KbReleaseWait( whichKeyboard ); % continues when key is released
  
  % Calc and show reaction time
  timeReaction = timeResponse - timeStimulus;
  
  s2ms = 1000;
  disp( ['Reaction Time: ', num2str( timeReaction * s2ms ), ' ms'] )
  
  %% Cleanup
  cleanUp( keys.on, audioHandle );
  
catch errorMessage
  %% Cleanup if crashed
  cleanUp( keys.on, audioHandle );
  rethrow( errorMessage );
end

%% Cleanup
ListenChar( keys.on ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows

%% Functions
function cleanUp( keyArgument, paHandle )
  ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
  Screen( 'CloseAll' ); % closes all open PTB windows
  PsychPortAudio('Stop', paHandle);  % Stop playback
  PsychPortAudio('Close', paHandle); % Close the audio device
end