% keyPress.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)


%% Settings
Screen('Preference', 'SkipSyncTests', 1); % option for hardware tests
whichScreen = 0;
whichKeyboard = -1;
keys.off = 2;
keys.on = 0;
load( '../colorHelper.mat' ) % predefined colors

try
  %% Window
  [mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen ); % opens PTB window
  screenWidth = windowRect(3);
  screenHeight = windowRect(4);
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Drawoptions
  % Sets options for items to be drawn
  
  %% Shows items
  Screen( 'Flip', mainWindow ); % "Flips" prepared scene onto the screen
  
  %% Pause
  KbWait( whichKeyboard ); % halts program until any key is pressed
  KbReleaseWait( whichKeyboard ); % continues when key is released
  
  %% Cleanup
  cleanUp( keys.on );
  
catch errorMessage
  %% Cleanup if crashed
  cleanUp( keys.on );
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end