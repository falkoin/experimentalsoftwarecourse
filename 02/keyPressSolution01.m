% keyPressSolution02.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)


%% Settings
Screen('Preference', 'SkipSyncTests', 1);
whichScreen = 0;
whichKeyboard = -1;
keys.off = 2;
keys.on = 0;
load( '../colorHelper.mat' ) % predefined colors

%% Experiment settings
delayTimes = 0.5 : 0.25 : 1.5; % possible random foreperiods
nDelays = numel( delayTimes ); % check number of elements
randomDelay = delayTimes(randi( nDelays )); % pick a random foreperiod value

try
  %% Window
  [mainWindow, windowRext] = Screen( 'OpenWindow', whichScreen ); % opens PTB window
  screenWidth = windowRext(3);
  screenHeight = windowRext(4);
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Drawoptions
  % Sets options for items to be drawn
  rectSize = [0, 0, 100, 100];
  relativeWidth = 0.5;
  relativeHeight = 0.5;
  myRect = CenterRectOnPoint( rectSize, screenWidth * relativeWidth, screenHeight * relativeHeight );
  Screen( 'FillRect', mainWindow, color.black, myRect );
  
  WaitSecs( randomDelay ); % random foreperiod
  
  %% Shows items
  timeStimulus = Screen( 'Flip', mainWindow ); % "Flips" prepared scene onto the screen
  
  %% Pause
  timeResponse = KbWait( whichKeyboard ); % halts program until any key is pressed
  KbReleaseWait( whichKeyboard ); % continues when key is released
  
  % Calc and show reaction time
  timeReaction = timeResponse - timeStimulus;
  
  s2ms = 1000;
  disp( ['Reaction Time: ', num2str( timeReaction * s2ms ), ' ms'] )
  
  %% Cleanup
  cleanUp( keys.on );
  
catch errorMessage
  %% Cleanup if crashed
  cleanUp( keys.on );
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end
