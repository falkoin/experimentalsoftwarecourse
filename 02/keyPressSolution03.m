% keyPressSolution04.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings

% Debug
isDebug = 1;
Screen('Preference', 'SkipSyncTests', 1);
keys.off = 2;
keys.on = 0;
whichScreen = 0;
whichKeyboard = -1;
load( '../colorHelper.mat' ) % predefined colors

%% Input
if isDebug == 0
  subjectInfo.nr         = input( 'Bitte geben Sie die VPN-Nummer ein: ');
  subjectInfo.subject    = input( 'Bitte geben Sie das VPN-K�rzel ein (Bsp.: Sheldon Cooper = SC): ', 's' );
  subjectInfo.supervisor = input( 'Bitte geben Sie Ihr VL-K�rzel ein: ', 's' );
  subjectInfo.age        = input( 'Bitte geben Sie das Alter der VPN an: ' );
  subjectInfo.gender     = input( 'Bitte geben Sie das Geschlecht der VPN an (w: 1, m: 2, d: 3): ' );
end

%% Experiment settings
delayTimes = 0.5 : 0.25 : 1.5; % possible random foreperiods
nDelays = numel( delayTimes ); % check number of elements
nTrials = 3;
nReactiontimes = nan( 1, nTrials );

try
  %% Window
  [mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen ); % opens PTB window
  screenWidth = windowRect(3);
  screenHeight = windowRect(4);
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Audiooptions
  % Sets options for items to be played
  audioVolume = 0.1;
  [audioData, audioFrequency] = psychwavread( 'beep-02.wav' ); % load soundfile
  waveData = audioData'; % convert audiodata
  
  % checks if the file was mono or stereo. If it's mono the channel will be doubled to fake stereo
  nChannels = size( waveData, 1 );
  if nChannels < 2 % if mono
    waveData = [waveData; waveData]; % make stereo
    nChannels = 2;
  end
  
  %% Audio initialization
  InitializePsychSound();
  latencySetting = 2;
  audioHandle = PsychPortAudio( 'Open', [], [], latencySetting, audioFrequency, 2 ); % create an audio handle
  
  PsychPortAudio( 'FillBuffer', audioHandle, waveData );
  PsychPortAudio( 'Volume', audioHandle, audioVolume );
  
  %% Text options
  text.font = 'Arial';
  text.size = 30;
  text.style = 0;
  Screen( 'TextFont', mainWindow, text.font );
  Screen( 'TextSize', mainWindow, text.size );
  Screen( 'TextStyle', mainWindow, text.style );
  
  myIntroduction1 = 'Reaktionszeit-Experiment';
  myIntroduction2 = 'Bitten legen Sie Ihren Finger auf die Leertaste.';
  myIntroduction3 = 'Ihnen wird nach einer kurzen Wartezeit ein Ton pr�sentiert.';
  myIntroduction4 = 'Sobald Sie den Ton h�ren, dr�cken Sie bitte so schnell es geht die Taste.';
  myIntroduction5 = ['Sie werden ', num2str( nTrials ), ' Versuche durchf�hren.'];
  lineBreak       = '\n';
  
  %% Introduction
  relativeHeight = 6/16;
  verticalSpacing = 2;
  DrawFormattedText( mainWindow, [myIntroduction1, lineBreak, myIntroduction2, lineBreak, ...
                     myIntroduction3, lineBreak, myIntroduction4, lineBreak, ...
                     myIntroduction5], 'center', screenHeight * relativeHeight, color.black, [], [], [], verticalSpacing );
  
  relativeHeight = 0.9;
  DrawFormattedText( mainWindow, 'Weiter mit beliebiger Taste.', 'center', screenHeight * relativeHeight, color.black );
  Screen( 'Flip', mainWindow );
  KbWait( whichKeyboard ); % waits for user input
  KbReleaseWait( whichKeyboard );
  Screen( 'Flip', mainWindow );
  
  %% Experiment Loop
  for currentTrial = 1 : nTrials
    
    % Plays items
    randomDelay = delayTimes(randi( nDelays )); % pick a random foreperiod value
    WaitSecs( randomDelay );
    timeStimulus = PsychPortAudio( 'Start', audioHandle, 1, 0, 1 );
    
    % Waits for reaction
    timeResponse = KbWait( whichKeyboard ); % halts program until any key is pressed
    KbReleaseWait( whichKeyboard ); % continues when key is released
    
    % Calc and show reaction time
    timeReaction = timeResponse - timeStimulus;
    nReactiontimes(currentTrial) = timeReaction;
    
    s2ms = 1000;
    relativeHeight = 4/6;
    DrawFormattedText( mainWindow, ['Reaktionszeit: ' num2str( timeReaction * s2ms, '%3.0f' ) ' ms'], ...
                       'center', screenHeight * relativeHeight, color.black );
    relativeHeight = 0.9;
    DrawFormattedText( mainWindow, 'Weiter mit beliebiger Taste.', 'center', screenHeight * relativeHeight, color.black );
    Screen( 'Flip', mainWindow );
    
    KbWait( whichKeyboard ); % waits for user input
    KbReleaseWait( whichKeyboard );
    Screen( 'Flip', mainWindow );
  end
  
  %% Cleanup
  cleanUp( keys.on, audioHandle );
  
catch errorMessage
  %% Cleanup if crashed
  cleanUp( keys.on, audioHandle );
  rethrow( errorMessage );
end

%% Data Export
if isDebug == 0
  save( ['data/' num2str( subjectInfo.nr, '%02d' ), '_', subjectInfo.subject, '_results.mat'], 'nReactiontimes', 'subjectInfo' );
end

%% Functions
function cleanUp( keyArgument, paHandle )
  ListenChar( keyArgument ); % keyboard is reactivated in MATLAB
  Screen( 'CloseAll' ); % closes all open PTB windows
  PsychPortAudio('Stop', paHandle);  % Stop playback
  PsychPortAudio('Close', paHandle); % Close the audio device:
end