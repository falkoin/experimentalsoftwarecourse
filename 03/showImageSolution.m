% showImageSolution.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings
whichScreen = 0;
whichKeybaord = -1;
load( '../colorHelper.mat' ) % predefined colors
keys.off = 2;
keys.on = 0;

%% Debug
Screen( 'Preference', 'SkipSyncTests', 1 );

%% Load images
imageFolder  = './images'; % image path
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
imageList = {imageList(:).name}; % saves only the names in a cell
nImages = numel(imageList);

%% Open Window
try
[mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen, color.black );
screenWidth = windowRect(3);
screenHeight = windowRect(4);
Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
                 % outside of the PTB window

randomImagePosition = randi( nImages ); % pick random position within the imagelist
randomImage = imageList{randomImagePosition}; % pick the correspondign imaage
expImage = imread( fullfile( imageFolder, randomImage ) ); % reads the image
expTexture = Screen( 'MakeTexture', mainWindow, expImage ); % creates a displayable texture

%% Show image
Screen( 'DrawTexture', mainWindow, expTexture ); % draw the texture
Screen( 'Flip', mainWindow ); % flips the texture onto the screen

%% Waits for input
KbWait( whichKeybaord );
KbReleaseWait( whichKeybaord );

%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end


%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end