% makeTrialsSolution.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

rng(1337) % set random to a fixed value
imageFolder = './images/';
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending

possibleItems      = 1 : numel( {imageList.name} ); % find out how many images we have
repititionPerTrial = 2; % every image should be onscreen 2 times
allTrials          = repmat( possibleItems, 1, repititionPerTrial ); % create a schedule with all images x2
mySchedule         = Shuffle( allTrials ); % shuffle the schedule to get randomness

save( 'experimentSchedule.mat', 'mySchedule' );
