whichScreen = 0;
whichKeybaord = -1;
turned.on = 0;
turned.off = 2;
nFrames = 0;
color.black = [0, 0, 0];
color.green = [0, 255, 0];

%% Debug
Screen('Preference', 'SkipSyncTests', 1);

try
  [mainWindow, windowSize] = Screen( 'OpenWindow', whichScreen, color.black );
  screenWidth = windowSize(3);
  screenHeight = windowSize(4);
  Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
  ListenChar( turned.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window

  pauseInSeconds = 1;
  dontClearFrameBuffer = 1;
  
  %% Text options
  textFont = 'Arial';
  textSize = 30;
  textStyle = 0;
  Screen( 'TextFont', mainWindow, textFont );
  Screen( 'TextSize', mainWindow, textSize );
  Screen( 'TextStyle', mainWindow, textStyle );
  
  %% Load images
  imageFolder  = './images/'; % image path
  imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
  imageList = {imageList(:).name}; % saves only the names in a cell
  loadedImages.first   = imread( fullfile(imageFolder, imageList{1} ) ); % reads a random image
  loadedImages.second   = imread( fullfile(imageFolder, imageList{6} ) ); % reads a random image
  textures.first = Screen( 'MakeTexture', mainWindow, loadedImages.first ); % creates a displayable texture
  textures.second = Screen( 'MakeTexture', mainWindow, loadedImages.second ); % creates a displayable texture
  textureSize = [0, 0, 1024, 768];
  texturePosition.first = CenterRectOnPoint( textureSize, screenWidth * 0.1, screenHeight * 0.5 );
  texturePosition.second = CenterRectOnPoint( textureSize, screenWidth * 0.6, screenHeight * 0.5 );
  
  DrawFormattedText( mainWindow, 'Draw two textures', 'center', 'center', color.green );
  Screen( 'Flip', mainWindow );

  WaitSecs( pauseInSeconds );

  Screen( 'DrawTexture', mainWindow, textures.first, [], texturePosition.first );
  Screen( 'DrawTexture', mainWindow, textures.second, [], texturePosition.second );
  Screen( 'Flip', mainWindow, [], dontClearFrameBuffer );
 
  WaitSecs( pauseInSeconds );
    
  Screen( 'Close', textures.first );
  DrawFormattedText( mainWindow, 'Draw two textures', 'center', 'center', color.green );
  Screen( 'Flip', mainWindow, [], dontClearFrameBuffer );
  
  
  WaitSecs( pauseInSeconds );
  
catch errorMessage
  %% Cleanup if crashed
  ListenChar( turned.on ); % keyboard is reactivated in MATLAB
  Screen( 'CloseAll' ); % closes all open PTB windows
  rethrow( errorMessage );
end

%% Cleanup
ListenChar( turned.on ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows