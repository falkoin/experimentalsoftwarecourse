% expressionSelectionSolution.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)


%% Settings
whichScreen = 0;
whichKeybaord = -1;
keys.off = 2;
keys.on = 0;
% colors 
load( '../colorHelper.mat' ) % predefined colors

%% Debug
Screen( 'Preference', 'SkipSyncTests', 1 );

%% Image Settings
% Load images
imageFolder  = './images/'; % image path
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
imageList = {imageList(:).name}; % saves only the names in a cell
nImages = numel(imageList);

%% Open Window
try
  [mainWindow, windowSize] = Screen( 'OpenWindow', whichScreen, color.black );
  screenWidth = windowSize(3);
  screenHeight = windowSize(4);
  Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Experiment settings
  flipInterval = Screen( 'GetFlipInterval', mainWindow ); % get the display refreshrate in ms
  timeStimulus = 0.2; % stimulus should be 200 ms onscreen
  timeStimulusInFrames = round( timeStimulus / flipInterval ); % real onscreen time
  timeStimulusReal = timeStimulusInFrames * flipInterval;
  
  %% Load images
  randomImagePosition = randi( nImages ); % pick random position within the imagelist
  randomImage = imageList{randomImagePosition}; % pick the correspondign imaage
  expImage = imread( fullfile( imageFolder, randomImage ) ); % reads the image
  expTexture = Screen( 'MakeTexture', mainWindow, expImage ); % creates a displayable texture
  
  nFrames = 1;
  isRunning = true;
  
  while isRunning
    Screen( 'DrawTexture', mainWindow, expTexture );
    if nFrames == 1
      timeFirstFlip = Screen( 'Flip', mainWindow ); % get the time for the first flip
    else
      Screen( 'Flip', mainWindow ); % we still want to control every flip
    end
    nFrames = nFrames + 1;
    if nFrames > timeStimulusInFrames % when the stimulus was long enough onscreen - abort
      isRunning = false;
    end
  end
  Screen( 'FillRect', mainWindow, color.black ); % creates a black screen
  timeSecondFlip = Screen( 'Flip', mainWindow ); % gets the time when the black screen appears/image disappears
  timeImageShown = timeSecondFlip - timeFirstFlip;
  fprintf('Achieved onscreen time: %s, Intended onscreen time: %s\n', num2str( timeImageShown ), num2str( timeStimulusReal ));
  
%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end


%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end