filename = 'test.txt';
fileID = fopen(filename);
data = textscan( fileID, '%s' );
fclose( fileID );

count = 0;
colorCount = 1;
entryCount = 1;
myColors = {};
while count < length( data{1,1} )
  count = count+1;
  myColors{colorCount, entryCount} = data{1,1}{count};
  entryCount = entryCount + 1;
  if startsWith(data{1,1}{count}(1), '(')
    colorCount = colorCount + 1;
    entryCount = 1;
  end
end