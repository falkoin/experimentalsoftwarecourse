% makeTrials.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

imageFolder = './images/';
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending

possibleItems = 1 : numel( {imageList.name} ); % creates a vector with all possible items
%% ...
% save( 'experimentSchedule.mat', 'mySchedule' );
