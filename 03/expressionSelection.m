% expressionSelection.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings
whichScreen = 0;
whichKeybaord = -1;
keys.off = 2;
keys.on = 0;
% colors
load( '../colorHelper.mat' ) % predefined colors

%% Debug
Screen( 'Preference', 'SkipSyncTests', 1 );

%% Image Settings
% Load images
imageFolder  = './images/'; % image path
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
imageList = {imageList(:).name}; % saves only the names in a cell
nImages = numel(imageList);

%% Open Window
try
[mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen, color.black );
screenWidth = windowRect(3);
screenHeight = windowRect(4);
Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
                 % outside of the PTB window

%% Experiment settings
flipInterval = Screen( 'GetFlipInterval', mainWindow ); % get the display refreshrate in ms
timeStimulus = 0.2; % stimulus should be 200 ms onscreen
%% ...

randomImagePosition = randi( nImages ); % pick random position within the imagelist
randomImage = imageList{randomImagePosition}; % pick the correspondign imaage
expImage = imread( fullfile( imageFolder, randomImage ) ); % reads the image
expTexture = Screen( 'MakeTexture', mainWindow, expImage ); % creates a displayable texture

%% Experiment start
%% ...

%% Validation
%% ...
fprintf('Achieved onscreen time: %s, Intended onscreen time: %s\n', num2str( timeImageShown ), num2str( timeStimulusReal ));
    
%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end


%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end