% expressionSelectionSolution2.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings
load( 'experimentSchedule.mat' );
whichScreen = 0;
whichKeybaord = -1;
keys.off = 2;
keys.on = 0;
% colors
load( '../colorHelper.mat' ) % predefined colors

%% Debug
Screen('Preference', 'SkipSyncTests', 1);
isDebug = true;

%% Image Settings
% Load images
imageFolder  = './images/'; % image path
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
imageList = {imageList(:).name}; % saves only the names in a cell
nImages = numel(imageList);

%% Experiment Settings
waitTimes = 1.5 : 0.25 : 2.5;
nWaitTimes = numel( waitTimes );
if isDebug
  nTrials = 2;
else
  nTrials = length( mySchedule );
end

%% Open Window
try
  [mainWindow, windowSize] = Screen( 'OpenWindow', whichScreen, color.black );
  screenWidth = windowSize(3);
  screenHeight = windowSize(4);
  Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Experiment settings
  flipInterval = Screen( 'GetFlipInterval', mainWindow ); % get the display refreshrate in ms
  timeStimulus = 0.2; % stimulus should be 200 ms onscreen
  timeStimulusInFrames = round( timeStimulus / flipInterval ); % real onscreen time
  timeStimulusReal = timeStimulusInFrames * flipInterval;
  
  %% Text options
  textFont = 'Arial';
  textSize = 30;
  textStyle = 0;
  Screen( 'TextFont', mainWindow, textFont );
  Screen( 'TextSize', mainWindow, textSize );
  Screen( 'TextStyle', mainWindow, textStyle );

  for i = 1 : nTrials
    %% Select image
    selectedImage = mySchedule(i);
    if selectedImage > nImages * 0.5
      expressionType = 1;
    else
      expressionType = 2;
    end
    expImage   = imread( fullfile(imageFolder, imageList{selectedImage} ) ); % reads a random image
    expTexture = Screen( 'MakeTexture', mainWindow, expImage ); % creates a displayable texture
    
    nFrames = 1;
    isRunning = true;
    
    randomWaitTime = waitTimes(randi( nWaitTimes ));
    WaitSecs( randomWaitTime ); % peseudo random foreperiod
    
    while isRunning
      Screen( 'DrawTexture', mainWindow, expTexture );
      if nFrames == 1
        timeFirstFlip = Screen( 'Flip', mainWindow ); % get the time for the first flip
      else
        Screen( 'Flip', mainWindow ); % we still want to control every flip
      end
      nFrames = nFrames + 1;
      if nFrames > timeStimulusInFrames % when the stimulus was long enough onscreen - remove it
        isRunning = false;
      end
    end
    
    Screen( 'FillRect', mainWindow, color.black ); % creates a black screen
    timeSecondFlip = Screen( 'Flip', mainWindow ); % time when the black screen appears/image disappears
    timeResponse = KbWait( whichKeybaord );
    KbReleaseWait( whichKeybaord );
    timeReaction = timeResponse - timeFirstFlip;
    heightMargin = 4/6;
    DrawFormattedText( mainWindow, ['Reaktionszeit: ' num2str( timeReaction * 1000, '%3.0f' ) ' ms'], ...
                       'center', screenHeight * heightMargin, color.white );
    heightMargin = 0.9;
    DrawFormattedText( mainWindow, 'Weiter mit beliebiger Taste.', 'center', screenHeight * heightMargin, ...
                       color.white );
    Screen( 'Flip', mainWindow );
    
    KbWait( whichKeybaord ); % wait for user input
    KbReleaseWait( whichKeybaord );
    Screen( 'Flip', mainWindow );
    
    timeImageShown = timeSecondFlip - timeFirstFlip;
    if isDebug
      fprintf('Achieved onscreen time: %s, Intended onscreen time: %s\n', num2str( timeImageShown ), num2str( timeStimulusReal ));
    end
  end
  
%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end


%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end