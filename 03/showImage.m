% showImage.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings
whichScreen = 0;
whichKeybaord = -1;
load( '../colorHelper.mat' ) % predefined colors
keys.off = 2;
keys.on = 0;

%% Debug
Screen('Preference', 'SkipSyncTests', 1);

%% Open Window
try
[mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen, color.black );
screenWidth = windowRect(3);
screenHeight = windowRect(4);
Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
                 % outside of the PTB window

%% Load images
%% ...

%% Show image
%% ...

%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end


%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end