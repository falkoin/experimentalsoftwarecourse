% expressionSelectionSolution3.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)

%% Settings
load( 'experimentSchedule.mat' );
whichScreen = 0;
whichKeybaord = -1;
s2ms = 1000;
keys.on = 0;
keys.off = 2;
dontClearFrameBuffer = 1;
% colors
load( '../colorHelper.mat' ) % predefined colors

%% Debug
Screen('Preference', 'SkipSyncTests', 1);
isDebug = true;

%% Image Settings
% Load images
imageFolder  = './images/'; % image path
imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
imageList = {imageList(:).name}; % saves only the names in a cell
nImages = numel(imageList);

%% Input
if ~isDebug
  myExp.nr         = input( 'Bitte geben Sie die VPN-Nummer ein: ');
  myExp.group      = input( 'Bitte geben Sie die Gruppe ein (1: f-j, 2: j-f): ');
  myExp.subject    = input( 'Bitte geben Sie das VPN-K�rzel ein (Bsp.: Sheldon Cooper = SC): ', 's' );
  myExp.supervisor = input( 'Bitte geben Sie Ihr VL-K�rzel ein: ', 's' );
  myExp.age        = input( 'Bitte geben Sie das Alter der VPN an: ' );
  myExp.gender     = input( 'Bitte geben Sie das Geschlecht der VPN an (w: 1,  m: 2, d: 3): ' );
else
  myExp.group = 1;
end

%% Experiment Settings pre Open
if myExp.group == 1
  myKeys = {'f', 'j'};
elseif myExp.group == 2
  myKeys = {'j', 'f'};
else
  error( 'Selected wrong group. Should be 1 or 2' );
end
key.left = 1;
key.right = 2;
stimulus.is.happy = 2;
stimulus.is.angry = 1;

myKeyCodes = [KbName( myKeys{key.left} ), KbName( myKeys{key.right} )];

nTrials = 2;
waitTimes = 1.5 : 0.25 : 2.5;
nWaitTimes = numel( waitTimes );
reactionTimes = nan( 1, nTrials );

%% Text options
textFont = 'Arial';
textSize = 30;
textStyle = 0;


%% Open Window
try
  [mainWindow, windowSize] = Screen( 'OpenWindow', whichScreen, color.black );
  screenWidth = windowSize(3);
  screenHeight = windowSize(4);
  Screen( 'BlendFunction', mainWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ); % optimal display function
  Screen( 'TextFont', mainWindow, textFont );
  Screen( 'TextSize', mainWindow, textSize );
  Screen( 'TextStyle', mainWindow, textStyle );
  ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear
  % outside of the PTB window
  
  %% Experiment settings post Open
  flipInterval = Screen( 'GetFlipInterval', mainWindow ); % get the display refreshrate in ms
  timeStimulus = 0.4; % stimulus should be 200 ms onscreen
  timeStimulusInFrames = round( timeStimulus / flipInterval ); % real onscreen time
  timeStimulusReal = timeStimulusInFrames * flipInterval;
  
  
  
  %% Load images
  imageFolder  = './images/'; % image path
  imageList = dir( fullfile( imageFolder, '*.jpg' ) ); % gets all files with jpg ending
  imageList = {imageList(:).name}; % saves only the names in a cell
  
  
  for i = 1 : nTrials
    %% Select image
    selectedImage = mySchedule(i);
    if selectedImage > nImages * 0.5
      expressionType = stimulus.is.angry;
    else
      expressionType = stimulus.is.happy;
    end
    expImage   = imread( fullfile(imageFolder, imageList{selectedImage} ) ); % reads a random image
    expTexture = Screen( 'MakeTexture', mainWindow, expImage ); % creates a displayable texture
    
    nFrames      = 1;
    isRunning    = true;
    showStimulus = true;
    
    randomWaitTime = waitTimes(randi( nWaitTimes ));
    WaitSecs( randomWaitTime ); % peseudo random foreperiod
    
    while isRunning
      if showStimulus
        Screen( 'DrawTexture', mainWindow, expTexture );
      end
      if nFrames == 1
        timeFirstFlip = Screen( 'Flip', mainWindow ); % get the time for the first flip
      else
        Screen( 'Flip', mainWindow, [], dontClearFrameBuffer ); % we still want to control every flip
      end
      [keyIsDown, timeResponse, keyCode] = KbCheck( whichKeybaord );
      if keyCode(myKeyCodes(key.left))
        isRunning = false;
        if expressionType == stimulus.is.angry
          myFeedback = 1;
        else
          myFeedback = 2;
        end
      elseif keyCode(myKeyCodes(key.right))
        isRunning = false;
        if expressionType == stimulus.is.happy
          myFeedback = 1;
        else
          myFeedback = 2;
        end
      end
      nFrames = nFrames + 1;
      if nFrames > timeStimulusInFrames && showStimulus % when the stimulus was long enough onscreen - remove it
        Screen( 'FillRect', mainWindow, color.black ); % creates a black screen
        showStimulus = false;
        timeSecondFlip = Screen( 'Flip', mainWindow ); % time when the black screen appears/image disappears
      end
    end
    Screen( 'FillRect', mainWindow, color.black ); % creates a black screen
    
    KbReleaseWait( whichKeybaord );
    
    timeReaction = timeResponse - timeFirstFlip; % calculates reaction time
    reactionTimes(i) = timeReaction;
    
    if myFeedback == 1
      DrawFormattedText( mainWindow, 'Richtig!', 'center', 'center', color.green );
    elseif myFeedback == 2
      DrawFormattedText( mainWindow, 'Falsch', 'center', 'center', color.red );
    else
      error( 'Feedback-Variable falsch gesetzt' );
    end
    
    heightMargin = 4/6;
    DrawFormattedText( mainWindow, ['Reaktionszeit: ' num2str( timeReaction * s2ms, '%3.0f' ) ' ms'], ...
                       'center', screenHeight * heightMargin, color.white );
    heightMargin = 0.9;
    DrawFormattedText( mainWindow, 'Weiter mit beliebiger Taste.', 'center', screenHeight * heightMargin, ...
                       color.white );
    Screen( 'Flip', mainWindow );
    KbWait( whichKeybaord ); % wair for user input
    KbReleaseWait( whichKeybaord );
    Screen( 'Flip', mainWindow );
    
    timeImageShown = timeSecondFlip - timeFirstFlip;
    if isDebug
      fprintf('Achieved onscreen time: %s, Intended onscreen time: %s\n', num2str( timeImageShown ), num2str( timeStimulusReal ));
    end
  end
  
%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

if ~isDebug
  save( [num2str( myExp.nr, '%02d' ), '_', myExp.subject, '_results.mat'], 'reactionTimes', 'myExp' );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
end