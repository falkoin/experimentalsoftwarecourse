%% Settings
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isDebug = 0;
syncMode = 2; % 0 = sync, 1 = hybrid, 2 = no sync
isRunning = 1;
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];
targetSize = 100;
baseRectTarget = [0, 0, targetSize, targetSize];

%% Helper Variables
% labels
button.left = 1;
button.right = 2;
button.middle = 3;

keys.off = 2;
keys.on = 0;

% colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( 0 );

% text
text.debug.size = 18;

try
%% Open Window
Screen( 'Preference', 'SkipSyncTests', isDebug );
[window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor, windowSize );
screenWidth = screenSize(3);
screenHeigth = screenSize(4);
Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
ListenChar( keys.off ); % prevents accidentally keyboard inputs
HideCursor(); % hides the mouse cursor

%% Experiment Settings
targetHeight = screenHeigth * 0.5;
relativeMargin.left = 0.2;
relativeMargin.right = 0.8;

centeredRect.leftTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.left, ...
                                                  targetHeight );
centeredRect.rightTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.right, ...
                                                   targetHeight );   

while isRunning
  [x, y, buttons] = GetMouse();
  
  if buttons(button.right)
    isRunning = 0;
  end
  % draw targets
  Screen( 'FrameRect', window, color.black, centeredRect.leftTarget );                                    
  Screen( 'FrameRect', window, color.black, centeredRect.rightTarget );
  
  % draw cursor
  centeredRect.cursor = CenterRectOnPoint( baseRectCursor, x, y );
  Screen( 'FillOval', window, color.black, centeredRect.cursor );
  
  if isDebug
    debugText = {'Debug:', ...
                 ['x = ' num2str( x )]};
    drawDebug( window, debugText, text.debug.size, color.black );
  end
  
  Screen( 'Flip', window, [], [], syncMode );
end

%% Cleanup
cleanUp( keys.on )
catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end

function drawDebug( window, debugText, textSize, color )
  Screen( 'TextSize', window, textSize );
        xTextPos = 0;
  for entry = 1 : length( debugText )
      yTextPos = entry + (textSize-1) * entry;
      DrawFormattedText( window, debugText{entry}, xTextPos, yTextPos, color );
  end
end