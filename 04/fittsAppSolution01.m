%% Settings
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isDebug = 0;
syncMode = 2; % 0 = sync, 1 = hybrid, 2 = no sync
isRunning = 1;
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];

%% Helper Variables
% labels
button.left = 1;
button.right = 2;
button.middle = 3;

keys.off = 2;
keys.on = 0;

% colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( 0 );

% text
text.debug.size = 18;

%% Open Window
Screen( 'Preference', 'SkipSyncTests', isDebug )

try
  [window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor );
  screenWidth = screenSize(3);
  screenHeigth = screenSize(4);
  Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
  ListenChar( keys.off ); % prevents accidentally keyboard inputs
  HideCursor(); % hides the mouse cursor
  
  %% Main loop 
  while isRunning
    tic
    [x, y, buttons] = GetMouse(); % gets mouse coordinates
    
    if buttons(button.right) % checks whether a mouse button was pressed and quits program
      isRunning = 0;
    end
    
    centeredRect = CenterRectOnPointd( baseRectCursor, x, y ); % creates a rect at the given position. x, y is center of the rect
    Screen( 'FillOval', window, color.black, centeredRect ); % creates a circle within the rect
    
    if isDebug % debug output
      debugEntries = {'Debug:', ...
                      ['x = ' num2str( x )], ...
                      ['y = ' num2str( y )]};
      drawDebug( window, debugEntries, text.debug.size, color.black )
    end
    
    Screen( 'Flip', window, [], [], syncMode );
    toc
  end
%% Cleanup
cleanUp( keys.on )
catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end




%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end

function drawDebug( window, debugText, textSize, color )
Screen( 'TextSize', window, textSize );
xTextPos = 0; % left side
for entry = 1 : length( debugText )
  yTextPos = entry + (textSize-1) * entry; % defines y-position for each entry
  DrawFormattedText( window, debugText{entry}, xTextPos, yTextPos, color ); % create text for each entry
end
end