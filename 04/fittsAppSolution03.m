%% Settings
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isDebug = 1;
syncMode = 0; % 0 = sync, 1 = hybrid, 2 = no sync
isRunning = 1;
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];
targetSize = 100;
baseRectTarget = [0, 0, targetSize, targetSize];

%% Helper Variables
% labels
button.left = 1;
button.right = 2;
button.middle = 3;

keys.off = 2;
keys.on = 0;

% colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( 0 );

% text
text.debug.size = 18;
text.labels.size = 40;

try
%% Open Window
Screen( 'Preference', 'SkipSyncTests', isDebug );
[window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor, windowSize );
Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
screenWidth = screenSize(3);
screenHeigth = screenSize(4);
ListenChar( keys.off ); % prevents accidentally keyboard inputs
HideCursor(); % hides the mouse cursor

%% Experiment Settings
centerScreen.y = screenSize(4) * 0.5;
centerScreen.x  = screenSize(3) * 0.5;
printError = 0;
relativeMargin.left = 0.2;
relativeMargin.right = 0.8;

centeredRect.leftTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.left, ...
                                                  centerScreen.y );
centeredRect.rightTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.right, ...
                                                   centerScreen.y );   

% save the boundaries in more meaningful variables
leftTarget.leftBoundary = centeredRect.leftTarget(1);
leftTarget.rightBoundary = centeredRect.leftTarget(3);

rightTarget.leftBoundary = centeredRect.rightTarget(1);
rightTarget.rightBoundary = centeredRect.rightTarget(3);
while isRunning
  [x, ~, buttons] = GetMouse();
  
  if buttons(button.right) % exit buttons
    isRunning = 0;
  end
  
  % draw targets
  Screen( 'FrameRect', window, color.black, centeredRect.leftTarget );                                    
  Screen( 'FrameRect', window, color.black, centeredRect.rightTarget );
  
  % draw cursor
  centeredRect.cursor = CenterRectOnPoint( baseRectCursor, x, centerScreen.y );
  Screen( 'FillOval', window, color.black, centeredRect.cursor );
  
  if isDebug
    debugText = {'Debug:', ...
                 ['x = ' num2str( x )], ...
                 ['printError = ' num2str( printError )]};
    drawDebug( window, debugText, text.debug.size, color.black );
  end
  
  if ~buttons(button.left)
    printError = 0;
  end
  
  %% Testing
  if buttons(button.left)
    if x > leftTarget.leftBoundary && x < leftTarget.rightBoundary
       Screen( 'DrawText', window, 'LEFT', centerScreen.x, centerScreen.y, color.green );
    elseif x > rightTarget.leftBoundary && x < rightTarget.rightBoundary
       Screen( 'DrawText', window, 'RIGHT', centerScreen.x, centerScreen.y, color.green );
    else
      printError = 1;
    end
  end
  
  if printError
    Screen( 'DrawText', window, 'ERROR', centerScreen.x, centerScreen.y, color.red );
  end
  Screen( 'Flip', window, [], [], syncMode );
end
%% Cleanup
cleanUp( keys.on )
catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end

function drawDebug( window, debugText, textSize, color )
  Screen( 'TextSize', window, textSize );
        xTextPos = 0;
  for entry = 1 : length( debugText )
      yTextPos = entry + (textSize-1) * entry;
      DrawFormattedText( window, debugText{entry}, xTextPos, yTextPos, color );
  end
end