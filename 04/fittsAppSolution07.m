% -------------------------------------------------------------------------
% Course "Praktische Einf�hrung in die Entwicklung von Experimentalsoftware",
% Worksheet 06, Solution for: 6 - 02 - c)
%
% Written in 2020 by Falko D�hring <falko.doehring@sport.uni-giessen.de>
%
%
% Licensed under the Attribution 4.0 International License (CC-BY-4.0);
% find the license text at <https://creativecommons.org/licenses/by/4.0/>.
% You are free to share and adapt this work if you give appropriate credit,
% provide a link to the license, and indicate if changes were made.
% -------------------------------------------------------------------------


%% Input
myExp.nr         = input( 'Bitte geben Sie die VPN-Nummer ein: ');
myExp.subject    = input( 'Bitte geben Sie das VPN-K�rzel ein (Bsp.: Sheldon Cooper = SC): ', 's' );
myExp.supervisor = input( 'Bitte geben Sie Ihr VL-K�rzel ein: ', 's' );
myExp.age        = input( 'Bitte geben Sie das Alter der VPN an: ' );
myExp.gender     = input( 'Bitte geben Sie das Geschlecht der VPN an (w: 1, m: 2, d: 3): ' );
myExp.group      = input( 'Bitte geben Sie die Gruppe an (ID 4: 1,  ID 6: 2): ' );

%% Settings
whichKeyboard = -1;
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isDebug = 1;
syncMode = 0; % 0 = sync, 1 = hybrid, 2 = no sync
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];

%% Helper Variables
% labels
button.left = 1;
button.right = 2;
button.middle = 3;

keys.off = 2;
keys.on = 0;

clicked.left = 1;
clicked.right = 2;
clickLabel = {'left', 'right', 'not clicked yet'};

% colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( 0 );

% text
text.debug.size = 18;
text.labels.size = 40;

try
%% Open Window
Screen( 'Preference', 'SkipSyncTests', isDebug );
[window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor, windowSize );
Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
flipInterval = Screen( 'GetFlipInterval', window );
screenWidth = screenSize(3);
screenHeigth = screenSize(4);
ListenChar( keys.off ); % prevents accidentally keyboard inputs
HideCursor(); % hides the mouse cursor

%% Experiment Settings
centerScreen.y = screenHeigth * 0.5;
centerScreen.x  = screenWidth * 0.5;
relativeMargin.left = 0.2;
relativeMargin.right = 0.8;
maxClicks = 11;
clickSeries = zeros( 1, maxClicks );
errorTime = 1; % in s
errorTimeInFrames = (errorTime*flipInterval)*1000;

nTrials = 2;

centerPosition.x.leftTarget = screenWidth*relativeMargin.left; % center of left target
centerPosition.x.rightTarget = screenWidth*relativeMargin.right; % center of right target



%% calculate target size
fitts.distance = centerPosition.x.rightTarget - centerPosition.x.leftTarget;
if myExp.group == 1
  fitts.id = 4;
  targetSize = fitts.distance / ((2^fitts.id) - 1);
elseif myExp.group == 2
  fitts.id = 6;
  targetSize = fitts.distance / ((2^fitts.id) - 1);
else
  error( 'Unexpected group found - can''t compute ID' );
end
baseRectTarget = [0, 0, targetSize, targetSize];

centeredRect.leftTarget = CenterRectArrayOnPoint( baseRectTarget, centerPosition.x.leftTarget, ...
                                                  centerScreen.y );
centeredRect.rightTarget = CenterRectArrayOnPoint( baseRectTarget, centerPosition.x.rightTarget, ...
                                                   centerScreen.y );   

% saves the boundaries in more meaningful variables
leftTarget.leftBoundary = centeredRect.leftTarget(1);
leftTarget.rightBoundary = centeredRect.leftTarget(3);

rightTarget.leftBoundary = centeredRect.rightTarget(1);
rightTarget.rightBoundary = centeredRect.rightTarget(3);

%% Introduction
% Text options
Screen( 'TextFont', window,'Arial' );
Screen( 'TextSize', window, 30 );
Screen( 'TextStyle', window, 0 );

% Text
myIntroductionCell = {'Fitts-Experiment', ...
                      'Ihre Aufgabe ist es, so schnell wie m�glich, unter Einhaltung der Genauigkeit,', ...
                      'zwischen den beiden Zielen hin- und herzuspringen.', ...
                      'Bitte klicken Sie die linke Maustaste beim Erreichen jedes Ziels.', ...
                      'Um zu Starten, klicken Sie in eines der beiden Ziele.', ...
                      ['Sie werden ', num2str( nTrials ), ' Versuche durchf�hren.']};
lineBreak = '\n';

% Creation
myIntroductionText = [];
for i = 1 : length( myIntroductionCell )
  myIntroductionText = [myIntroductionText, myIntroductionCell{i}, lineBreak];
end

textMargin = 6 / 16;
DrawFormattedText( window, myIntroductionText, 'center', screenHeigth * textMargin, color.black, [], [], [], 2 );

textMargin = 0.9;
DrawFormattedText( window, 'Weiter mit beliebiger Taste.', 'center', screenHeigth * textMargin, color.black );
Screen( 'Flip', window );
KbWait( whichKeyboard ); % wair for user input
KbReleaseWait();
Screen( 'Flip', window );


for currentTrial = 1 : nTrials
  %% Experiment Settings for each trial
  clickTime = nan( 1, maxClicks );
  isRunning = 1;
  lastClick = 3;
  whereClicked = 3;
  isClickable = 1;
  nClicks = 0;
  printError = 0;
  errorTimer = 1;
  
  while isRunning
    [x, ~, buttons] = GetMouse();
    if ~buttons(button.left)
      isClickable = 1;
    end

    if buttons(button.right) || nClicks >= maxClicks % exit buttons
      isRunning = 0;
    end

    % draws targets
    Screen( 'FrameRect', window, color.black, centeredRect.leftTarget );                                    
    Screen( 'FrameRect', window, color.black, centeredRect.rightTarget );

    % draws cursor
    centeredRect.cursor = CenterRectOnPoint( baseRectCursor, x, centerScreen.y );
    Screen( 'FillOval', window, color.black, centeredRect.cursor );

    if isDebug
      debugText = {'Debug:', ...
                   ['x = ' num2str( x )], ...
                   ['printError = ' num2str( printError )], ...
                   ['timeStamp = ' num2str( clickTime )], ...
                   ['isClickable = ' num2str( isClickable )], ...
                   ['nClicks = ' num2str( nClicks )], ...
                   ['lastClick = ' clickLabel{lastClick}], ...
                   ['whereClicked = ' clickLabel{whereClicked}]};
      drawDebug( window, debugText, text.debug.size, color.black );
    end

    if ~buttons(button.left) && errorTimer > errorTimeInFrames
      printError = 0;
      errorTimer = 0;
    end

    %% Testing
    if buttons(button.left)
      if x > leftTarget.leftBoundary && x < leftTarget.rightBoundary
         whereClicked = clicked.left; % memorize the clicked field
         if whereClicked == lastClick % check if the click was in the same field as before
           isClickable = 0; % don't allow to click
         else
           lastClick = whereClicked; % memorize the clicked field for the next check
         end
         if isClickable
           isClickable = 0;
           nClicks = nClicks + 1;
           clickTime(nClicks) = flipTime;
         end
      elseif x > rightTarget.leftBoundary && x < rightTarget.rightBoundary
         whereClicked = clicked.right; % memorize the clicked field
         if whereClicked == lastClick
           isClickable = 0;
         else
           lastClick = whereClicked;
         end
          if isClickable
            isClickable = 0;
            nClicks = nClicks + 1;
            clickTime(nClicks) = flipTime;
         end
      else
        printError = 1;
      end
    end

    if printError && errorTimer < errorTimeInFrames
      Screen( 'DrawText', window, 'ERROR', centerScreen.x, centerScreen.y, color.red );
      errorTimer = errorTimer+1;
    end
    flipTime = Screen( 'Flip', window, [], [], syncMode );
  end
  movementTime = diff( clickTime ); % generate movementTime by using diff
  save( [num2str( myExp.nr, '%02d' ), '_', num2str(currentTrial, '%02d' ), '_', myExp.subject, '_results.mat'], 'movementTime', 'myExp' );
  
  %% End trial message
  Screen( 'Flip', window );
  preResultTime = 2;
  WaitSecs( preResultTime );
  meanMT = mean( movementTime );
  Screen( 'TextSize', window, text.labels.size );
  DrawFormattedText( window, ['Mean Movement Time: ', num2str( meanMT, 3 ), ' s'], 'center', 'center', color.black );
  Screen( 'Flip', window );
  afterTrialWaitTime = 5;
  WaitSecs( afterTrialWaitTime );
  
end
%% Cleanup
cleanUp( keys.on )
catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end

function drawDebug( window, debugText, textSize, color )
  Screen( 'TextSize', window, textSize );
        xTextPos = 0;
  for entry = 1 : length( debugText )
      yTextPos = entry + (textSize-1) * entry;
      DrawFormattedText( window, debugText{entry}, xTextPos, yTextPos, color );
  end
end