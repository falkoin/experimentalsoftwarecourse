% -------------------------------------------------------------------------
% Course "Praktische Einf�hrung in die Entwicklung von Experimentalsoftware",
% Worksheet 06, Solution for: 6 - 01 - c)
%
% Written in 2020 by Falko D�hring <falko.doehring@sport.uni-giessen.de>
%
%
% Licensed under the Attribution 4.0 International License (CC-BY-4.0);
% find the license text at <https://creativecommons.org/licenses/by/4.0/>.
% You are free to share and adapt this work if you give appropriate credit,
% provide a link to the license, and indicate if changes were made.
% -------------------------------------------------------------------------

%% Settings
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isDebug = 1;
syncMode = 0; % 0 = sync, 1 = hybrid, 2 = no sync
isRunning = 1;
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];
targetSize = 100;
baseRectTarget = [0, 0, targetSize, targetSize];

%% Helper Variables
% labels
button.left = 1;
button.right = 2;
button.middle = 3;

keys.off = 2;
keys.on = 0;

clicked.left = 1;
clicked.right = 2;

% colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( 0 );

% text
text.debug.size = 18;
text.labels.size = 40;

try
%% Open Window
Screen( 'Preference', 'SkipSyncTests', isDebug );
[window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor, windowSize );
Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
screenWidth = screenSize(3);
screenHeigth = screenSize(4);
ListenChar( keys.off ); % prevents accidentally keyboard inputs
HideCursor(); % hides the mouse cursor

%% Experiment Settings
centerScreen.y = screenHeigth * 0.5;
centerScreen.x  = screenWidth * 0.5;
printError = 0;
relativeMargin.left = 0.2;
relativeMargin.right = 0.8;
isClickable = 1;
nClicks = 0;
maxClicks = 11;
clickTime = nan( 1, maxClicks );
clickSeries = zeros( 1, maxClicks );
lastClick = 0;
whereClicked = 0;


centeredRect.leftTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.left, ...
                                                  centerScreen.y );
centeredRect.rightTarget = CenterRectArrayOnPoint( baseRectTarget, screenWidth*relativeMargin.right, ...
                                                   centerScreen.y );   

% saves the boundaries in more meaningful variables
leftTarget.leftBoundary = centeredRect.leftTarget(1);
leftTarget.rightBoundary = centeredRect.leftTarget(3);

rightTarget.leftBoundary = centeredRect.rightTarget(1);
rightTarget.rightBoundary = centeredRect.rightTarget(3);
while isRunning
  [x, ~, buttons] = GetMouse();
  if ~buttons(button.left)
    isClickable = 1;
  end
  
  if buttons(button.right) || nClicks >= 11 % exit buttons
    isRunning = 0;
  end
  
  % draws targets
  Screen( 'FrameRect', window, color.black, centeredRect.leftTarget );                                    
  Screen( 'FrameRect', window, color.black, centeredRect.rightTarget );
  
  % draws cursor
  centeredRect.cursor = CenterRectOnPoint( baseRectCursor, x, centerScreen.y );
  Screen( 'FillOval', window, color.black, centeredRect.cursor );
  
  if isDebug
    debugText = {'Debug:', ...
                 ['x = ' num2str( x )], ...
                 ['printError = ' num2str( printError )], ...
                 ['timeStamp = ' num2str( clickTime )], ...
                 ['isClickable = ' num2str( isClickable )], ...
                 ['nClicks = ' num2str( nClicks )], ...
                 ['lastClick = ' num2str( lastClick )], ...
                 ['whereClicked = ' num2str( whereClicked )]};
    drawDebug( window, debugText, text.debug.size, color.black );
  end
  
  if ~buttons(button.left)
    printError = 0;
  end
  
  %% Testing
  if buttons(button.left)
    if x > leftTarget.leftBoundary && x < leftTarget.rightBoundary
       Screen( 'DrawText', window, 'LEFT', centerScreen.x, centerScreen.y, color.green );
       whereClicked = clicked.left; % memorize the clicked field
       if whereClicked == lastClick % check if the click was in the same field as before
         isClickable = 0; % don't allow to click
       else
         lastClick = whereClicked; % memorize the clicked field for the next check
       end
       if isClickable
         isClickable = 0;
         nClicks = nClicks + 1;
         clickTime(nClicks) = flipTime;
       end
    elseif x > rightTarget.leftBoundary && x < rightTarget.rightBoundary
       Screen( 'DrawText', window, 'RIGHT', centerScreen.x, centerScreen.y, color.green );
       whereClicked = clicked.right; % memorize the clicked field
       if whereClicked == lastClick
         isClickable = 0;
       else
         lastClick = whereClicked;
       end
        if isClickable
          isClickable = 0;
          nClicks = nClicks + 1;
          clickTime(nClicks) = flipTime;
       end
    else
      printError = 1;
    end
  end
  
  if printError
    Screen( 'DrawText', window, 'ERROR', centerScreen.x, centerScreen.y, color.red );
  end
  flipTime = Screen( 'Flip', window, [], [], syncMode );
end
%% Cleanup
cleanUp( keys.on )
catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end

function drawDebug( window, debugText, textSize, color )
  Screen( 'TextSize', window, textSize );
        xTextPos = 0;
  for entry = 1 : length( debugText )
      yTextPos = entry + (textSize-1) * entry;
      DrawFormattedText( window, debugText{entry}, xTextPos, yTextPos, color );
  end
end