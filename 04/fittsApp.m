% fittsApp.m
% (c) Falko Döhring (falko.doehring@sport.uni-giessen.de)

% ###########################################################################
% ###########################       WARNING       ########################### 
% ###########################################################################
% ## THIS PROGRAM IS AN ENDLESS LOOP WITHOUT IMPLEMENTING AN EXIT CONDITION #
% ###########################################################################
% ###########################   CHECK WORKSHEET!  ########################### 

%% Settings
Screen('Preference', 'SkipSyncTests', 1);
whichScreen = 0;
windowSize = [0, 0, 1280, 800];

isRunning = 1;
cursorSize = 10;
baseRectCursor = [0, 0, cursorSize, cursorSize];

%% Experiment Variables
nTrials = 1;

%% Helper Variables
% Colors
load( '../colorHelper.mat' ) % predefined colors
color.backgroundColor = WhiteIndex( whichScreen );
% Labels
keys.off = 2;
keys.on = 0;
mousebutton2 = 2;


%% Open Window
try
  [window, screenSize] = Screen( 'OpenWindow', whichScreen, color.backgroundColor, windowSize );
  screenWidth = screenSize(3);
  screenHeigth = screenSize(4);
  Screen( 'BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );
  ListenChar( keys.off ); % prevents accidentally keyboard inputs
  HideCursor(); % hides the mouse cursor
  


  %% Main loop
  while isRunning
    Screen( 'Flip', window );
  end
  
%% Cleanup
cleanUp( keys.on )
  
catch errorMessage
  %% Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage );
end

%% Functions
function cleanUp( keyArgument )
ListenChar( keyArgument  ); % keyboard is reactivated in MATLAB
Screen( 'CloseAll' ); % closes all open PTB windows
ShowCursor();
end
