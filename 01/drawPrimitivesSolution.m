% drawPrimitives.m
% (c) Falko D�hring (falko.doehring@sport.uni-giessen.de)


%% Settings
keys.off = 2;
keys.on = 0;
skipTest = 1;
whichScreen = 0; % decides which screen to use for the PTB window
whichKeyboard = -1; % decides which keyboard to use (e.g. extern or intern kb). -1 = try all
screenSize = [0, 0, 1280, 800]; % window size, can be used as parameter for OpenWindow
load( '../colorHelper.mat' ) % predefined colors

%% Drawing
lineWidth = 2;

%% Window
try
Screen( 'Preference', 'SkipSyncTests', skipTest ); % supresses PTB tests to improve starting reliability on non experimental machines
ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear outside of the PTB window
[window, windowSize] = Screen( 'OpenWindow', whichScreen, [], screenSize ); % opens PTB window 
screenWidth = windowRect(3);
screenHeight = windowRect(4);

%% Primitves-Options
% draw line
myLine.xStart = 1;
myLine.yStart = 1;
myLine.xEnd   = screenWidth;
myLine.yEnd   = screenHeight;

rectSize = [0, 0, 100, 100]; % size of the base rect

% positions for filled objects
rectPosition.x = screenWidth * 0.5;
rectPosition.y = screenWidth * 0.5;

ovalPosition.x = screenWidth * 0.3;
ovalPosition.y = screenHeight * 0.5;

% places rects at the configured positions
myRect = CenterRectOnPoint( rectSize, rectPosition.x, rectPosition.y );
myOval = CenterRectOnPoint( rectSize, ovalPosition.x, ovalPosition.y );

% positions for framed objects
frameRectPosition.x = windowSize(3) * 0.6;
frameRectPosition.y = screenHeight * 0.5;

frameOvalPosition.x = windowSize(3) * 0.2;
frameOvalPosition.y = screenHeight * 0.5;

% places rects at the configured positions
myFrameRect = CenterRectOnPoint( rectSize, frameRectPosition.x, frameRectPosition.y );
myFrameOval = CenterRectOnPoint( rectSize, frameOvalPosition.x, frameOvalPosition.y );

% construciton of bonus
myRadius = 60;
nSides = 6; % Number of sides for our polygon

cornerAngles = linspace(-90, 270, nSides + 1) * (pi / 180);

xValues = cos( cornerAngles' ) .* myRadius + screenWidth * 0.7;
yValues = sin( cornerAngles' ) .* myRadius + screenHeight * 0.5;

%% Prepare primitives
Screen( 'DrawLine', mainWindow, color.teal, myLine.xStart, myLine.yStart, myLine.xEnd, myLine.yEnd, lineWidth );
Screen( 'FillRect', mainWindow, color.blue, myRect );
Screen( 'FillOval', mainWindow, color.blue, myOval );
Screen( 'FillPoly', mainWindow, color.blue, [xValues(1:end-1), yValues(1:end-1)] );

Screen( 'FrameRect', mainWindow, color.magenta, myFrameRect, lineWidth );
Screen( 'FrameOval', mainWindow, color.magenta, myFrameOval, lineWidth );
Screen( 'FramePoly', mainWindow, color.magenta, [xValues(1:end-1), yValues(1:end-1)], lineWidth );

%% Shows graphics
Screen( 'Flip', window ); % "Flips" prepared scene onto the screen

%% Pause
KbWait( whichKeyboard ); % halts program until any key is pressed
KbReleaseWait( whichKeyboard ); % continues when key is released

%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage )
end

%% Functions
function cleanUp( keyArgument )
  ListenChar( keyArgument ); % keyboard is reactivated in MATLAB
  Screen( 'CloseAll' ); % closes all open PTB windows (sca does the same in short)
end
