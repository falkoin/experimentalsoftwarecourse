% drawPrimitives.m
% (c) Falko Döhring (falko.doehring@sport.uni-giessen.de)


%% Settings
keys.off = 2;
keys.on = 0;
skipTest = 1;
whichScreen = 0; % decides which screen to use for the PTB window
whichKeyboard = -1; % decides which keyboard to use (e.g. extern or intern kb). -1 = try all
screenSize = [0, 0, 1280, 800]; % window size, can be used as parameter for OpenWindow
load( '../colorHelper.mat' ) % predefined colors

%% Window
try
Screen( 'Preference', 'SkipSyncTests', skipTest ); % supresses PTB tests to improve starting reliability on non experimental machines
ListenChar( keys.off ); % disables keyboard in MATLAB, not PTB - pressed keys will not appear outside of the PTB window
[mainWindow, windowRect] = Screen( 'OpenWindow', whichScreen, [], screenSize ); % opens PTB window 
screenWidth = windowRect(3);
screenHeight = windowRect(4);

%% Prepare primitives

%% Shows graphics
Screen( 'Flip', mainWindow ); % "Flips" prepared scene onto the screen

%% Pause
KbWait( whichKeyboard ); % halts program until any key is pressed
KbReleaseWait( whichKeyboard ); % continues when key is released

%% Cleanup
cleanUp( keys.on )

catch errorMessage
  % Cleanup if crashed
  cleanUp( keys.on )
  rethrow( errorMessage )
end

%% Functions
function cleanUp( keyArgument )
  ListenChar( keyArgument ); % keyboard is reactivated in MATLAB
  Screen( 'CloseAll' ); % closes all open PTB windows (sca does the same in short)
end
