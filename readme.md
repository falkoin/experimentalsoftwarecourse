# Praktische Einführung in die Entwicklung von Experimentalsoftware

## 1. Einführung in die Psychtoolbox (PTB)

[Präsentation](01/01_Einfuehrung.pdf)

[Ordner](01/)

[Arbeitsblatt](01/01_worksheet.pdf)

## 2. Einstieg in die Methoden der Psychophysik: Tastenabfragen, Ton, Reaktionszeiten (Erstellung eines einfachen Reaktionszeit-Experiments)

[Präsentation](02/02_Grundlagen.pdf)

[Ordner](02/)

[Arbeitsblatt](02/worksheet02.pdf)

## 3. Erweiterte Kontrolle bei Experimenten: Bilder, Validierung und fortschrittlichere Tastenabfragen (Erstellung eines "two choice" Emotions-Experiments)

[Ordner](03/)

[Arbeitsblatt](03/03_worksheet.pdf)

## 4. Kontinuierliche Experimente: Fitts’ (Erstellung eines Fitts-Experiments)

[Arbeitsblatt](04/04_worksheet.pdf)

[Präsentation](04/04_Experimentalsoftware.pdf)

[Ordner](04/)
